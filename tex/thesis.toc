\contentsline {chapter}{\numberline {1}مقدمه}{11}{chapter.1}
\contentsline {section}{\numberline {1-1}تعریف مسئله}{11}{section.1.1}
\contentsline {section}{\numberline {1-2}فرضیات و مشکلات موجود}{12}{section.1.2}
\contentsline {section}{\numberline {1-3}اهمیت موضوع}{13}{section.1.3}
\contentsline {section}{\numberline {1-4}اهداف تحقیق}{13}{section.1.4}
\contentsline {section}{\numberline {1-5}ساختار پایان‌نامه}{13}{section.1.5}
\contentsline {chapter}{\numberline {2}ادبیات مربوطه}{14}{chapter.2}
\contentsline {section}{\numberline {2-1}مفاهیم اولیه}{14}{section.2.1}
\contentsline {subsection}{\numberline {2-1-1}شبکه های عصبی مصنوعی}{14}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2-1-2}شبکه های عصبی همگشتی}{15}{subsection.2.1.2}
\contentsline {section}{\numberline {2-2}کارهای پیشین}{15}{section.2.2}
\contentsline {subsection}{\numberline {2-2-1}قطعه‌بندی معنایی و استنتاج روابط پشتیبانی، سیلبرمن}{15}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2-2-2}استدلال سه بعدی به وسیله‌ی بلاک ها، پشتیبانی و پایداری‌ (Zhaoyin Jia)}{16}{subsection.2.2.2}
\contentsline {section}{\numberline {2-3}جمع بندی}{17}{section.2.3}
\contentsline {chapter}{\numberline {3}روش های پیشنهادی}{19}{chapter.3}
\contentsline {section}{\numberline {3-1}بهبود پیشبینی های میانی}{19}{section.3.1}
\contentsline {subsection}{\numberline {3-1-1}روش اولیه}{19}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3-1-2}روش‌های بهبود}{20}{subsection.3.1.2}
\contentsline {section}{\numberline {3-2}خصیصه‌های شبکه‌های عمیق}{21}{section.3.2}
\contentsline {subsection}{\numberline {3-2-1}خصیصه‌ها}{21}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3-2-2}مدل‌ها}{21}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3-2-3}روش‌های پیش‌پردازش تصاویر}{22}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3-2-4}فهرست خصیصه‌های تولید شده}{24}{subsection.3.2.4}
\contentsline {section}{\numberline {3-3}جمع بندی}{26}{section.3.3}
\contentsline {chapter}{\numberline {4}نتایج تجربی}{27}{chapter.4}
\contentsline {section}{\numberline {4-1}مجموعه‌های داده}{27}{section.4.1}
\contentsline {section}{\numberline {4-2}دقت بهبودهای مختلف روش اول}{28}{section.4.2}
\contentsline {section}{\numberline {4-3}مقایسه ی روش شبکه‌های عمیق}{28}{section.4.3}
\contentsline {section}{\numberline {4-4}جمع بندی}{29}{section.4.4}
\contentsline {chapter}{\numberline {5}جمع بندی و راه کار های آتی}{31}{chapter.5}
\contentsline {chapter}{\numberline {آ}مطالب تکمیلی}{32}{appendix.Alph1}
\contentsline {section}{\numberline {آ-1}جداول تکمیلی روش سیلبرمن}{32}{section.Alph1.1}
\contentsline {section}{\numberline {آ-2}پیاده‌سازی}{32}{section.Alph1.2}
